LIB = liblpbsa.a
OBJ = lpbsa.o

default : all

include Common.mk

all : $(LIB)

$(LIB) : $(OBJ)
	ar rs $(LIB) $(OBJ)

test : $(LIB)
	$(MAKE) -C test test

doc :
	$(MAKE) -C doc all

lpbsa : main.o $(LIB)
	$(CC) main.o -L. -llpbsa -lm -o lpbsa

clean :
	$(RM) $(LIB) $(OBJ) main.o lpbsa
	$(MAKE) -C test clean
	$(MAKE) -C doc clean
	$(MAKE) -C example clean


scan-build :
	scan-build -v -o /tmp make

.PHONY : default test doc clean scan-build
