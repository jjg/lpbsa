Example
-------

An example program using the `lpbsa` library, printing a
table of area values for a range of _p_.  Typical usage

    ./lpbsa-table 3 1 2 11

which takes the dimension to be 3 and _p_ in the range
1.0, 1.1, ..., 2.0.  See

    ./lbbsa-table --help

for a list of options.
