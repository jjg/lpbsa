/*
  lpbsa.h

  J.J. Green 2018
*/

#ifndef LPBSA_H
#define LPBSA_H

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

  typedef struct
  {
    bool short_circuit;
  } lpbsa_opt_t;

  extern double lpbsa(double, size_t, size_t);
  extern double lpbsa_long(double, size_t, size_t, lpbsa_opt_t*);

#ifdef __cplusplus
}
#endif

#endif
