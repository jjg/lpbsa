/*
  tests_lpbsa.h

  Copyright (c) J.J. Green 2018
*/

#ifndef TESTS_LPBSA_H
#define TESTS_LPBSA_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_lpbsa[];

extern void test_lpbsa_bad_dimension(void);
extern void test_lpbsa_bad_p(void);
extern void test_lpbsa_diamond_coarse(void);
extern void test_lpbsa_diamond_fine(void);
extern void test_lpbsa_disc(void);
extern void test_lpbsa_sphere(void);
extern void test_lpbsa_square_coarse(void);
extern void test_lpbsa_square_fine(void);
extern void test_lpbsa_cube_coarse(void);
extern void test_lpbsa_cube_fine(void);
extern void test_lpbsa_hypercube_coarse(void);
extern void test_lpbsa_hypercube_fine(void);
extern void test_lpbsa_short_circuit_1(void);
extern void test_lpbsa_short_circuit_2(void);
extern void test_lpbsa_short_circuit_inf(void);

#endif
