/*
  tests.c
  unit test case loader

  Copyright (c) J.J. Green 2018
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "cunit_compat.h"
#include "tests_lpbsa.h"

static CU_SuiteInfo suites[] =
  {
    CU_Suite_Entry("lpbsa", NULL, NULL, tests_lpbsa),
    CU_SUITE_INFO_NULL,
  };

void tests_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr, "suite registration failed - %s\n",
              CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
