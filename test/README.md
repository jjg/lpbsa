Unit tests
==========

These tests use the [CUnit library](http://cunit.sourceforge.net/),
version 2.1-3 or earlier: on Debian-based systems install the
`libcunit1-dev` package.
